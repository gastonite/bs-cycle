open Webapi.Dom;
open CycleRun;

type sources = {. "react": CycleReact.ReactSource.t};
type sinks = {. "react": CycleReact.Stream.Element_stream.t};

[@bs.val] external symbol: unit => Js_types.symbol = "Symbol";

let main: main(sources, sinks) =
  sources => {
    open CycleReact.Stream;
    let inc = symbol();
    let dec = symbol();
    let action =
      CycleReact.(
        Element_stream.merge([|
          sources##react
          ->select(dec)
          ->events("click")
          ->Element_stream.map(_ => (-1)),
          sources##react
          ->select(inc)
          ->events("click")
          ->Element_stream.map(_ => 1),
        |])
      );
    let count = action->Element_stream.fold((acc, x) => acc + x, 0);
    let vdom =
      CycleReactDom.H.(
        count->Element_stream.map(count =>
          div([|
            buttonSel(
              dec,
              ~props=
                props(
                  ~title="Increment",
                  ~style=ReactDOMRe.Style.make(~marginRight="10px", ()),
                  (),
                ),
              textContent("Decrement"),
            ),
            buttonSel(inc, textContent("Increment")),
            p(textContent("Counter: " ++ string_of_int(count))),
          |])
        )
      );
    {"react": vdom};
  };

run(
  main,
  {
    "react":
      CycleReactDom.makeDOMDriver(Document.getElementById("app", document)),
  },
);