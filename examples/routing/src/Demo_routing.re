open Cycle_dom_h;
open Cycle_dom_stream;
open Cycle_history_driver;
open CycleDom;
open CycleHistory;
open CycleRun;

type sources = {
  .
  "dom": domSource,
  "history": history_source,
};
type sinks = {
  .
  "dom": domSink,
  "history": HistoryStream.Sink_stream.t,
};

[@bs.deriving abstract]
type navNodeData = {
  [@bs.optional]
  page: string,
};

[@bs.get] external navDataSet: Dom.eventTarget => navNodeData = "dataset";

let makeNavData = (page, pathname) => {
  open Snabbdom;
  let classDict = Js.Dict.empty();
  let dataSetDict = Js.Dict.empty();
  Js.Dict.set(classDict, "active", "/" ++ page === pathname);
  Js.Dict.set(dataSetDict, "page", page);
  vData(~classNames=classDict, ~dataset=dataSetDict, ());
};

let navigation = pathname =>
  nav([|
    span(~data=makeNavData("home", pathname), textNodes("Home")),
    span(~data=makeNavData("about", pathname), textNodes("About")),
    span(~data=makeNavData("contacts", pathname), textNodes("Contacts")),
  |]);

let homePageView = () =>
  div([|
    h1(textNodes("Welcome to History Examples!")),
    p(textNodes("lorem ipsum")),
  |]);

let aboutPageView = () =>
  div([|h1(textNodes("About me")), p(textNodes("lorem ipsum"))|]);

let contactPageView = () =>
  div([|h1(textNodes("Contact me")), p(textNodes("lorem ipsum"))|]);

[@bs.scope "JSON"] [@bs.val]
external historyView: Location_stream.ok => string = "stringify";

let view = (history: history_source) =>
  Node_stream.(
    history->mapFromMemory(h => {
      let pathname = Location.pathname(h);
      let page =
        switch (pathname) {
        | "/home" => homePageView()
        | "/about" => aboutPageView()
        | "/contacts" => contactPageView()
        | _ => h1(textNodes("404 Not found"))
        };
      div([|
        navigation(pathname),
        page,
        br([||]),
        h3(textNodes("History object")),
        p(textNodes(historyView(h))),
      |]);
    })
  );

let main: main(sources, sinks) =
  sources => {
    open Node_stream;
    open Webapi.Dom.Event;
    let history =
      sources##dom
      ->select("nav")
      ->events("click")
      ->map(e => e->target->navDataSet->pageGet)
      ->map(page =>
          switch (page) {
          | Some(page) => page
          | None => ""
          }
        )
      ->compose(dropRepeats());

    {"dom": view(sources##history), "history": history};
  };

run(
  main,
  {
    "dom": makeDOMDriverWithSelector("#app", domDriverOptions()),
    "history": makeHashHistoryDriver(),
  },
);