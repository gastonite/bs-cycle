[@bs.module "@cycle/react"]
external makeComponent:
  (CycleRun.main('sources, 'sinks), 'sources) => ReasonReact.reactElement =
  "";

[@bs.deriving abstract]
type cycleReactEngine = {
  source: Cycle_react_source.ReactSource.t,
  sink: Cycle_react_source.sink,
  [@bs.optional]
  events: Js.Dict.t(Cycle_react_stream.Event_stream.t),
  [@bs.optional]
  dispose: CycleRun.disposeFunction,
};

type runOnDidMount = unit => Cycle_react_source.ReactSource.t;

[@bs.module "@cycle/react"]
external makeCycleReactComponent: runOnDidMount => ReasonReact.reactElement =
  "";