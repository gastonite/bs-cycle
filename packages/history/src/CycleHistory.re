include Cycle_history_capture;
include Cycle_history_types;
module HistoryDriver = Cycle_history_driver;
module History = Webapi.Dom.History;
module HistoryStream = Cycle_history_stream;
include Cycle_history_stream;
module Location = Webapi.Dom.Location;