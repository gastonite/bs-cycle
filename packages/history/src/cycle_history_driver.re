type history_source = Cycle_history_stream.Location_stream.t;

type getConfirmationCallback = bool => unit;
type getUserConfirmation =
  (~message: string, ~callback: getConfirmationCallback) => unit;

[@bs.deriving abstract]
type history_build_options = {
  basename: string,
  [@bs.optional]
  forceRefresh: bool,
  [@bs.optional]
  getUserConfirmation,
  [@bs.optional]
  keyLength: int,
};

[@bs.module "@cycle/history/lib/es6/drivers"]
external makeHistoryDriver:
  (~opts: history_build_options=?, unit) => history_source =
  "";

[@bs.module "@cycle/history/lib/es6/drivers"]
external makeHistoryDriverWithHistory: Webapi.Dom.History.t => history_source =
  "makeHistoryDriver";

[@bs.module "@cycle/history/lib/es6/drivers"]
external makeServerHistoryDriver: history_build_options => history_source = "";

[@bs.module "@cycle/history/lib/es6/drivers"]
external makeHashHistoryDriver:
  (~opts: history_build_options=?, unit) => history_source =
  "";