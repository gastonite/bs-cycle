open Xstream;

module Location_stream =
  Memory.Make_memory_stream({
    type ok = Webapi.Dom.Location.t;
    type error = Js.Exn.t;
  });

/* TODO: sink stream can take HistoryInput, GenericInput or string */
/* See Cycle_history_types and https://github.com/cyclejs/cyclejs/blob/master/history/src/types.ts */
module Sink_stream =
  Stream.Make_stream({
    type ok = string;
    type error = Js.Exn.t;
  });