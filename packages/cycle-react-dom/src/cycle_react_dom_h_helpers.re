let textContent = CycleReact.textContent;
let props = Cycle_react_dom_props.t;

type hDom =
  (
    ~css: string=?,
    ~props: Cycle_react_dom_props.t=?,
    array(ReasonReact.reactElement)
  ) =>
  ReasonReact.reactElement;

/* MVI helper */
type hDomMVI =
  (
    Js_types.symbol,
    ~props: Cycle_react_dom_props.t=?,
    array(ReasonReact.reactElement)
  ) =>
  ReasonReact.reactElement;

/* SVG */
/* [@bs.module "@cycle/react-dom"] external a: hDom = "a"; */
/* [@bs.module "@cycle/react-dom"] external aSel: hDomMVI = "a"; */
[@bs.module "@cycle/react-dom"] external altGlyph: hDom = "altGlyph";
[@bs.module "@cycle/react-dom"] external altGlyphSel: hDomMVI = "altGlyph";
[@bs.module "@cycle/react-dom"] external altGlyphDef: hDom = "altGlyphDef";
[@bs.module "@cycle/react-dom"]
external altGlyphDefSel: hDomMVI = "altGlyphDef";
[@bs.module "@cycle/react-dom"] external altGlyphItem: hDom = "altGlyphItem";
[@bs.module "@cycle/react-dom"]
external altGlyphItemSel: hDomMVI = "altGlyphItem";
[@bs.module "@cycle/react-dom"] external animate: hDom = "animate";
[@bs.module "@cycle/react-dom"] external animateSel: hDomMVI = "animate";
[@bs.module "@cycle/react-dom"] external animateColor: hDom = "animateColor";
[@bs.module "@cycle/react-dom"]
external animateColorSel: hDomMVI = "animateColor";
[@bs.module "@cycle/react-dom"] external animateMotion: hDom = "animateMotion";
[@bs.module "@cycle/react-dom"]
external animateMotionSel: hDomMVI = "animateMotion";
[@bs.module "@cycle/react-dom"]
external animateTransform: hDom = "animateTransform";
[@bs.module "@cycle/react-dom"]
external animateTransformSel: hDomMVI = "animateTransform";
[@bs.module "@cycle/react-dom"] external circle: hDom = "circle";
[@bs.module "@cycle/react-dom"] external circleSel: hDomMVI = "circle";
[@bs.module "@cycle/react-dom"] external clipPath: hDom = "clipPath";
[@bs.module "@cycle/react-dom"] external clipPathSel: hDomMVI = "clipPath";
[@bs.module "@cycle/react-dom"] external colorProfile: hDom = "colorProfile";
[@bs.module "@cycle/react-dom"]
external colorProfileSel: hDomMVI = "colorProfile";
[@bs.module "@cycle/react-dom"] external cursor: hDom = "cursor";
[@bs.module "@cycle/react-dom"] external cursorSel: hDomMVI = "cursor";
[@bs.module "@cycle/react-dom"] external defs: hDom = "defs";
[@bs.module "@cycle/react-dom"] external defsSel: hDomMVI = "defs";
[@bs.module "@cycle/react-dom"] external desc: hDom = "desc";
[@bs.module "@cycle/react-dom"] external descSel: hDomMVI = "desc";
[@bs.module "@cycle/react-dom"] external ellipse: hDom = "ellipse";
[@bs.module "@cycle/react-dom"] external ellipseSel: hDomMVI = "ellipse";
[@bs.module "@cycle/react-dom"] external feBlend: hDom = "feBlend";
[@bs.module "@cycle/react-dom"] external feBlendSel: hDomMVI = "feBlend";
[@bs.module "@cycle/react-dom"] external feColorMatrix: hDom = "feColorMatrix";
[@bs.module "@cycle/react-dom"]
external feColorMatrixSel: hDomMVI = "feColorMatrix";
[@bs.module "@cycle/react-dom"]
external feComponentTransfer: hDom = "feComponentTransfer";
[@bs.module "@cycle/react-dom"]
external feComponentTransferSel: hDomMVI = "feComponentTransfer";
[@bs.module "@cycle/react-dom"] external feComposite: hDom = "feComposite";
[@bs.module "@cycle/react-dom"]
external feCompositeSel: hDomMVI = "feComposite";
[@bs.module "@cycle/react-dom"]
external feConvolveMatrix: hDom = "feConvolveMatrix";
[@bs.module "@cycle/react-dom"]
external feConvolveMatrixSel: hDomMVI = "feConvolveMatrix";
[@bs.module "@cycle/react-dom"]
external feDiffuseLighting: hDom = "feDiffuseLighting";
[@bs.module "@cycle/react-dom"]
external feDiffuseLightingSel: hDomMVI = "feDiffuseLighting";
[@bs.module "@cycle/react-dom"]
external feDisplacementMap: hDom = "feDisplacementMap";
[@bs.module "@cycle/react-dom"]
external feDisplacementMapSel: hDomMVI = "feDisplacementMap";
[@bs.module "@cycle/react-dom"]
external feDistantLight: hDom = "feDistantLight";
[@bs.module "@cycle/react-dom"]
external feDistantLightSel: hDomMVI = "feDistantLight";
[@bs.module "@cycle/react-dom"] external feFlood: hDom = "feFlood";
[@bs.module "@cycle/react-dom"] external feFloodSel: hDomMVI = "feFlood";
[@bs.module "@cycle/react-dom"] external feFuncA: hDom = "feFuncA";
[@bs.module "@cycle/react-dom"] external feFuncASel: hDomMVI = "feFuncA";
[@bs.module "@cycle/react-dom"] external feFuncB: hDom = "feFuncB";
[@bs.module "@cycle/react-dom"] external feFuncBSel: hDomMVI = "feFuncB";
[@bs.module "@cycle/react-dom"] external feFuncG: hDom = "feFuncG";
[@bs.module "@cycle/react-dom"] external feFuncGSel: hDomMVI = "feFuncG";
[@bs.module "@cycle/react-dom"] external feFuncR: hDom = "feFuncR";
[@bs.module "@cycle/react-dom"] external feFuncRSel: hDomMVI = "feFuncR";
[@bs.module "@cycle/react-dom"]
external feGaussianBlur: hDom = "feGaussianBlur";
[@bs.module "@cycle/react-dom"]
external feGaussianBlurSel: hDomMVI = "feGaussianBlur";
[@bs.module "@cycle/react-dom"] external feImage: hDom = "feImage";
[@bs.module "@cycle/react-dom"] external feImageSel: hDomMVI = "feImage";
[@bs.module "@cycle/react-dom"] external feMerge: hDom = "feMerge";
[@bs.module "@cycle/react-dom"] external feMergeSel: hDomMVI = "feMerge";
[@bs.module "@cycle/react-dom"] external feMergeNode: hDom = "feMergeNode";
[@bs.module "@cycle/react-dom"]
external feMergeNodeSel: hDomMVI = "feMergeNode";
[@bs.module "@cycle/react-dom"] external feMorphology: hDom = "feMorphology";
[@bs.module "@cycle/react-dom"]
external feMorphologySel: hDomMVI = "feMorphology";
[@bs.module "@cycle/react-dom"] external feOffset: hDom = "feOffset";
[@bs.module "@cycle/react-dom"] external feOffsetSel: hDomMVI = "feOffset";
[@bs.module "@cycle/react-dom"] external fePointLight: hDom = "fePointLight";
[@bs.module "@cycle/react-dom"]
external fePointLightSel: hDomMVI = "fePointLight";
[@bs.module "@cycle/react-dom"]
external feSpecularLighting: hDom = "feSpecularLighting";
[@bs.module "@cycle/react-dom"]
external feSpecularLightingSel: hDomMVI = "feSpecularLighting";
[@bs.module "@cycle/react-dom"] external feSpotlight: hDom = "feSpotlight";
[@bs.module "@cycle/react-dom"]
external feSpotlightSel: hDomMVI = "feSpotlight";
[@bs.module "@cycle/react-dom"] external feTile: hDom = "feTile";
[@bs.module "@cycle/react-dom"] external feTileSel: hDomMVI = "feTile";
[@bs.module "@cycle/react-dom"] external feTurbulence: hDom = "feTurbulence";
[@bs.module "@cycle/react-dom"]
external feTurbulenceSel: hDomMVI = "feTurbulence";
[@bs.module "@cycle/react-dom"] external filter: hDom = "filter";
[@bs.module "@cycle/react-dom"] external filterSel: hDomMVI = "filter";
[@bs.module "@cycle/react-dom"] external font: hDom = "font";
[@bs.module "@cycle/react-dom"] external fontSel: hDomMVI = "font";
[@bs.module "@cycle/react-dom"] external fontFace: hDom = "fontFace";
[@bs.module "@cycle/react-dom"] external fontFaceSel: hDomMVI = "fontFace";
[@bs.module "@cycle/react-dom"]
external fontFaceFormat: hDom = "fontFaceFormat";
[@bs.module "@cycle/react-dom"]
external fontFaceFormatSel: hDomMVI = "fontFaceFormat";
[@bs.module "@cycle/react-dom"] external fontFaceName: hDom = "fontFaceName";
[@bs.module "@cycle/react-dom"]
external fontFaceNameSel: hDomMVI = "fontFaceName";
[@bs.module "@cycle/react-dom"] external fontFaceSrc: hDom = "fontFaceSrc";
[@bs.module "@cycle/react-dom"]
external fontFaceSrcSel: hDomMVI = "fontFaceSrc";
[@bs.module "@cycle/react-dom"] external fontFaceUri: hDom = "fontFaceUri";
[@bs.module "@cycle/react-dom"]
external fontFaceUriSel: hDomMVI = "fontFaceUri";
[@bs.module "@cycle/react-dom"] external foreignObject: hDom = "foreignObject";
[@bs.module "@cycle/react-dom"]
external foreignObjectSel: hDomMVI = "foreignObject";
[@bs.module "@cycle/react-dom"] external g: hDom = "g";
[@bs.module "@cycle/react-dom"] external gSel: hDomMVI = "g";
[@bs.module "@cycle/react-dom"] external glyph: hDom = "glyph";
[@bs.module "@cycle/react-dom"] external glyphSel: hDomMVI = "glyph";
[@bs.module "@cycle/react-dom"] external glyphRef: hDom = "glyphRef";
[@bs.module "@cycle/react-dom"] external glyphRefSel: hDomMVI = "glyphRef";
[@bs.module "@cycle/react-dom"] external hkern: hDom = "hkern";
[@bs.module "@cycle/react-dom"] external hkernSel: hDomMVI = "hkern";
[@bs.module "@cycle/react-dom"] external image: hDom = "image";
[@bs.module "@cycle/react-dom"] external imageSel: hDomMVI = "image";
[@bs.module "@cycle/react-dom"] external line: hDom = "line";
[@bs.module "@cycle/react-dom"] external lineSel: hDomMVI = "line";
[@bs.module "@cycle/react-dom"]
external linearGradient: hDom = "linearGradient";
[@bs.module "@cycle/react-dom"]
external linearGradientSel: hDomMVI = "linearGradient";
[@bs.module "@cycle/react-dom"] external marker: hDom = "marker";
[@bs.module "@cycle/react-dom"] external markerSel: hDomMVI = "marker";
[@bs.module "@cycle/react-dom"] external mask: hDom = "mask";
[@bs.module "@cycle/react-dom"] external maskSel: hDomMVI = "mask";
[@bs.module "@cycle/react-dom"] external metadata: hDom = "metadata";
[@bs.module "@cycle/react-dom"] external metadataSel: hDomMVI = "metadata";
[@bs.module "@cycle/react-dom"] external missingGlyph: hDom = "missingGlyph";
[@bs.module "@cycle/react-dom"]
external missingGlyphSel: hDomMVI = "missingGlyph";
[@bs.module "@cycle/react-dom"] external mpath: hDom = "mpath";
[@bs.module "@cycle/react-dom"] external mpathSel: hDomMVI = "mpath";
[@bs.module "@cycle/react-dom"] external path: hDom = "path";
[@bs.module "@cycle/react-dom"] external pathSel: hDomMVI = "path";
[@bs.module "@cycle/react-dom"] external pattern: hDom = "pattern";
[@bs.module "@cycle/react-dom"] external patternSel: hDomMVI = "pattern";
[@bs.module "@cycle/react-dom"] external polygon: hDom = "polygon";
[@bs.module "@cycle/react-dom"] external polygonSel: hDomMVI = "polygon";
[@bs.module "@cycle/react-dom"] external polyline: hDom = "polyline";
[@bs.module "@cycle/react-dom"] external polylineSel: hDomMVI = "polyline";
[@bs.module "@cycle/react-dom"]
external radialGradient: hDom = "radialGradient";
[@bs.module "@cycle/react-dom"]
external radialGradientSel: hDomMVI = "radialGradient";
[@bs.module "@cycle/react-dom"] external rect: hDom = "rect";
[@bs.module "@cycle/react-dom"] external rectSel: hDomMVI = "rect";
/* [@bs.module "@cycle/react-dom"] external script: hDom = "script"; */
/* [@bs.module "@cycle/react-dom"] external scriptSel: hDomMVI = "script"; */
[@bs.module "@cycle/react-dom"] external set: hDom = "set";
[@bs.module "@cycle/react-dom"] external setSel: hDomMVI = "set";
[@bs.module "@cycle/react-dom"] external stop: hDom = "stop";
[@bs.module "@cycle/react-dom"] external stopSel: hDomMVI = "stop";
/* [@bs.module "@cycle/react-dom"] external style: hDom = "style"; */
/* [@bs.module "@cycle/react-dom"] external styleSel: hDomMVI = "style"; */
[@bs.module "@cycle/react-dom"] external switch_: hDom = "switch";
[@bs.module "@cycle/react-dom"] external switchSel: hDomMVI = "switch";
[@bs.module "@cycle/react-dom"] external symbol: hDom = "symbol";
[@bs.module "@cycle/react-dom"] external symbolSel: hDomMVI = "symbol";
[@bs.module "@cycle/react-dom"] external text: hDom = "text";
[@bs.module "@cycle/react-dom"] external textSel: hDomMVI = "text";
[@bs.module "@cycle/react-dom"] external textPath: hDom = "textPath";
[@bs.module "@cycle/react-dom"] external textPathSel: hDomMVI = "textPath";
/* [@bs.module "@cycle/react-dom"] external title: hDom = "title"; */
/* [@bs.module "@cycle/react-dom"] external titleSel: hDomMVI = "title"; */
[@bs.module "@cycle/react-dom"] external tref: hDom = "tref";
[@bs.module "@cycle/react-dom"] external trefSel: hDomMVI = "tref";
[@bs.module "@cycle/react-dom"] external tspan: hDom = "tspan";
[@bs.module "@cycle/react-dom"] external tspanSel: hDomMVI = "tspan";
[@bs.module "@cycle/react-dom"] external use: hDom = "use";
[@bs.module "@cycle/react-dom"] external useSel: hDomMVI = "use";
[@bs.module "@cycle/react-dom"] external view: hDom = "view";
[@bs.module "@cycle/react-dom"] external viewSel: hDomMVI = "view";
[@bs.module "@cycle/react-dom"] external vkern: hDom = "vkern";
[@bs.module "@cycle/react-dom"] external vkernSel: hDomMVI = "vkern";

/* TAGS */
[@bs.module "@cycle/react-dom"] external a: hDom = "a";
[@bs.module "@cycle/react-dom"] external aSel: hDomMVI = "a";
[@bs.module "@cycle/react-dom"] external abbr: hDom = "abbr";
[@bs.module "@cycle/react-dom"] external abbrSel: hDomMVI = "abbr";
[@bs.module "@cycle/react-dom"] external address: hDom = "address";
[@bs.module "@cycle/react-dom"] external addressSel: hDomMVI = "address";
[@bs.module "@cycle/react-dom"] external area: hDom = "area";
[@bs.module "@cycle/react-dom"] external areaSel: hDomMVI = "area";
[@bs.module "@cycle/react-dom"] external article: hDom = "article";
[@bs.module "@cycle/react-dom"] external articleSel: hDomMVI = "article";
[@bs.module "@cycle/react-dom"] external aside: hDom = "aside";
[@bs.module "@cycle/react-dom"] external asideSel: hDomMVI = "aside";
[@bs.module "@cycle/react-dom"] external audio: hDom = "audio";
[@bs.module "@cycle/react-dom"] external audioSel: hDomMVI = "audio";
[@bs.module "@cycle/react-dom"] external b: hDom = "b";
[@bs.module "@cycle/react-dom"] external bSel: hDomMVI = "b";
[@bs.module "@cycle/react-dom"] external base: hDom = "base";
[@bs.module "@cycle/react-dom"] external baseSel: hDomMVI = "base";
[@bs.module "@cycle/react-dom"] external bdi: hDom = "bdi";
[@bs.module "@cycle/react-dom"] external bdiSel: hDomMVI = "bdi";
[@bs.module "@cycle/react-dom"] external bdo: hDom = "bdo";
[@bs.module "@cycle/react-dom"] external bdoSel: hDomMVI = "bdo";
[@bs.module "@cycle/react-dom"] external blockquote: hDom = "blockquote";
[@bs.module "@cycle/react-dom"] external blockquoteSel: hDomMVI = "blockquote";
[@bs.module "@cycle/react-dom"] external body: hDom = "body";
[@bs.module "@cycle/react-dom"] external bodySel: hDomMVI = "body";
[@bs.module "@cycle/react-dom"] external br: hDom = "br";
[@bs.module "@cycle/react-dom"] external brSel: hDomMVI = "br";
[@bs.module "@cycle/react-dom"] external button: hDom = "button";
[@bs.module "@cycle/react-dom"] external buttonSel: hDomMVI = "button";
[@bs.module "@cycle/react-dom"] external canvas: hDom = "canvas";
[@bs.module "@cycle/react-dom"] external canvasSel: hDomMVI = "canvas";
[@bs.module "@cycle/react-dom"] external caption: hDom = "caption";
[@bs.module "@cycle/react-dom"] external captionSel: hDomMVI = "caption";
[@bs.module "@cycle/react-dom"] external cite: hDom = "cite";
[@bs.module "@cycle/react-dom"] external citeSel: hDomMVI = "cite";
[@bs.module "@cycle/react-dom"] external code: hDom = "code";
[@bs.module "@cycle/react-dom"] external codeSel: hDomMVI = "code";
[@bs.module "@cycle/react-dom"] external col: hDom = "col";
[@bs.module "@cycle/react-dom"] external colSel: hDomMVI = "col";
[@bs.module "@cycle/react-dom"] external colgroup: hDom = "colgroup";
[@bs.module "@cycle/react-dom"] external colgroupSel: hDomMVI = "colgroup";
[@bs.module "@cycle/react-dom"] external dd: hDom = "dd";
[@bs.module "@cycle/react-dom"] external ddSel: hDomMVI = "dd";
[@bs.module "@cycle/react-dom"] external del: hDom = "del";
[@bs.module "@cycle/react-dom"] external delSel: hDomMVI = "del";
[@bs.module "@cycle/react-dom"] external dfn: hDom = "dfn";
[@bs.module "@cycle/react-dom"] external dfnSel: hDomMVI = "dfn";
[@bs.module "@cycle/react-dom"] external dir: hDom = "dir";
[@bs.module "@cycle/react-dom"] external dirSel: hDomMVI = "dir";
[@bs.module "@cycle/react-dom"] external div: hDom = "div";
[@bs.module "@cycle/react-dom"] external divSel: hDomMVI = "div";
[@bs.module "@cycle/react-dom"] external dl: hDom = "dl";
[@bs.module "@cycle/react-dom"] external dlSel: hDomMVI = "dl";
[@bs.module "@cycle/react-dom"] external dt: hDom = "dt";
[@bs.module "@cycle/react-dom"] external dtSel: hDomMVI = "dt";
[@bs.module "@cycle/react-dom"] external em: hDom = "em";
[@bs.module "@cycle/react-dom"] external emSel: hDomMVI = "em";
[@bs.module "@cycle/react-dom"] external embed: hDom = "embed";
[@bs.module "@cycle/react-dom"] external embedSel: hDomMVI = "embed";
[@bs.module "@cycle/react-dom"] external fieldset: hDom = "fieldset";
[@bs.module "@cycle/react-dom"] external fieldsetSel: hDomMVI = "fieldset";
[@bs.module "@cycle/react-dom"] external figcaption: hDom = "figcaption";
[@bs.module "@cycle/react-dom"] external figcaptionSel: hDomMVI = "figcaption";
[@bs.module "@cycle/react-dom"] external figure: hDom = "figure";
[@bs.module "@cycle/react-dom"] external figureSel: hDomMVI = "figure";
[@bs.module "@cycle/react-dom"] external footer: hDom = "footer";
[@bs.module "@cycle/react-dom"] external footerSel: hDomMVI = "footer";
[@bs.module "@cycle/react-dom"] external form: hDom = "form";
[@bs.module "@cycle/react-dom"] external formSel: hDomMVI = "form";
[@bs.module "@cycle/react-dom"] external h1: hDom = "h1";
[@bs.module "@cycle/react-dom"] external h1Sel: hDomMVI = "h1";
[@bs.module "@cycle/react-dom"] external h2: hDom = "h2";
[@bs.module "@cycle/react-dom"] external h2Sel: hDomMVI = "h2";
[@bs.module "@cycle/react-dom"] external h3: hDom = "h3";
[@bs.module "@cycle/react-dom"] external h3Sel: hDomMVI = "h3";
[@bs.module "@cycle/react-dom"] external h4: hDom = "h4";
[@bs.module "@cycle/react-dom"] external h4Sel: hDomMVI = "h4";
[@bs.module "@cycle/react-dom"] external h5: hDom = "h5";
[@bs.module "@cycle/react-dom"] external h5Sel: hDomMVI = "h5";
[@bs.module "@cycle/react-dom"] external h6: hDom = "h6";
[@bs.module "@cycle/react-dom"] external h6Sel: hDomMVI = "h6";
[@bs.module "@cycle/react-dom"] external head: hDom = "head";
[@bs.module "@cycle/react-dom"] external headSel: hDomMVI = "head";
[@bs.module "@cycle/react-dom"] external header: hDom = "header";
[@bs.module "@cycle/react-dom"] external headerSel: hDomMVI = "header";
[@bs.module "@cycle/react-dom"] external hgroup: hDom = "hgroup";
[@bs.module "@cycle/react-dom"] external hgroupSel: hDomMVI = "hgroup";
[@bs.module "@cycle/react-dom"] external hr: hDom = "hr";
[@bs.module "@cycle/react-dom"] external hrSel: hDomMVI = "hr";
[@bs.module "@cycle/react-dom"] external html: hDom = "html";
[@bs.module "@cycle/react-dom"] external htmlSel: hDomMVI = "html";
[@bs.module "@cycle/react-dom"] external i: hDom = "i";
[@bs.module "@cycle/react-dom"] external iSel: hDomMVI = "i";
[@bs.module "@cycle/react-dom"] external iframe: hDom = "iframe";
[@bs.module "@cycle/react-dom"] external iframeSel: hDomMVI = "iframe";
[@bs.module "@cycle/react-dom"] external img: hDom = "img";
[@bs.module "@cycle/react-dom"] external imgSel: hDomMVI = "img";
[@bs.module "@cycle/react-dom"] external input: hDom = "input";
[@bs.module "@cycle/react-dom"] external inputSel: hDomMVI = "input";
[@bs.module "@cycle/react-dom"] external ins: hDom = "ins";
[@bs.module "@cycle/react-dom"] external insSel: hDomMVI = "ins";
[@bs.module "@cycle/react-dom"] external kbd: hDom = "kbd";
[@bs.module "@cycle/react-dom"] external kbdSel: hDomMVI = "kbd";
[@bs.module "@cycle/react-dom"] external keygen: hDom = "keygen";
[@bs.module "@cycle/react-dom"] external keygenSel: hDomMVI = "keygen";
[@bs.module "@cycle/react-dom"] external label: hDom = "label";
[@bs.module "@cycle/react-dom"] external labelSel: hDomMVI = "label";
[@bs.module "@cycle/react-dom"] external legend: hDom = "legend";
[@bs.module "@cycle/react-dom"] external legendSel: hDomMVI = "legend";
[@bs.module "@cycle/react-dom"] external li: hDom = "li";
[@bs.module "@cycle/react-dom"] external liSel: hDomMVI = "li";
[@bs.module "@cycle/react-dom"] external link: hDom = "link";
[@bs.module "@cycle/react-dom"] external linkSel: hDomMVI = "link";
[@bs.module "@cycle/react-dom"] external main: hDom = "main";
[@bs.module "@cycle/react-dom"] external mainSel: hDomMVI = "main";
[@bs.module "@cycle/react-dom"] external map: hDom = "map";
[@bs.module "@cycle/react-dom"] external mapSel: hDomMVI = "map";
[@bs.module "@cycle/react-dom"] external mark: hDom = "mark";
[@bs.module "@cycle/react-dom"] external markSel: hDomMVI = "mark";
[@bs.module "@cycle/react-dom"] external menu: hDom = "menu";
[@bs.module "@cycle/react-dom"] external menuSel: hDomMVI = "menu";
[@bs.module "@cycle/react-dom"] external meta: hDom = "meta";
[@bs.module "@cycle/react-dom"] external metaSel: hDomMVI = "meta";
[@bs.module "@cycle/react-dom"] external nav: hDom = "nav";
[@bs.module "@cycle/react-dom"] external navSel: hDomMVI = "nav";
[@bs.module "@cycle/react-dom"] external noscript: hDom = "noscript";
[@bs.module "@cycle/react-dom"] external noscriptSel: hDomMVI = "noscript";
[@bs.module "@cycle/react-dom"] external object_: hDom = "object";
[@bs.module "@cycle/react-dom"] external objectSel: hDomMVI = "object";
[@bs.module "@cycle/react-dom"] external ol: hDom = "ol";
[@bs.module "@cycle/react-dom"] external olSel: hDomMVI = "ol";
[@bs.module "@cycle/react-dom"] external optgroup: hDom = "optgroup";
[@bs.module "@cycle/react-dom"] external optgroupSel: hDomMVI = "optgroup";
[@bs.module "@cycle/react-dom"] external option: hDom = "option";
[@bs.module "@cycle/react-dom"] external optionSel: hDomMVI = "option";
[@bs.module "@cycle/react-dom"] external p: hDom = "p";
[@bs.module "@cycle/react-dom"] external pSel: hDomMVI = "p";
[@bs.module "@cycle/react-dom"] external param: hDom = "param";
[@bs.module "@cycle/react-dom"] external paramSel: hDomMVI = "param";
[@bs.module "@cycle/react-dom"] external pre: hDom = "pre";
[@bs.module "@cycle/react-dom"] external preSel: hDomMVI = "pre";
[@bs.module "@cycle/react-dom"] external progress: hDom = "progress";
[@bs.module "@cycle/react-dom"] external progressSel: hDomMVI = "progress";
[@bs.module "@cycle/react-dom"] external q: hDom = "q";
[@bs.module "@cycle/react-dom"] external qSel: hDomMVI = "q";
[@bs.module "@cycle/react-dom"] external rp: hDom = "rp";
[@bs.module "@cycle/react-dom"] external rpSel: hDomMVI = "rp";
[@bs.module "@cycle/react-dom"] external rt: hDom = "rt";
[@bs.module "@cycle/react-dom"] external rtSel: hDomMVI = "rt";
[@bs.module "@cycle/react-dom"] external ruby: hDom = "ruby";
[@bs.module "@cycle/react-dom"] external rubySel: hDomMVI = "ruby";
[@bs.module "@cycle/react-dom"] external s: hDom = "s";
[@bs.module "@cycle/react-dom"] external sSel: hDomMVI = "s";
[@bs.module "@cycle/react-dom"] external samp: hDom = "samp";
[@bs.module "@cycle/react-dom"] external sampSel: hDomMVI = "samp";
[@bs.module "@cycle/react-dom"] external script: hDom = "script";
[@bs.module "@cycle/react-dom"] external scriptSel: hDomMVI = "script";
[@bs.module "@cycle/react-dom"] external section: hDom = "section";
[@bs.module "@cycle/react-dom"] external sectionSel: hDomMVI = "section";
/* Name clash with ReactSource.select */
[@bs.module "@cycle/react-dom"] external select_: hDom = "select";
[@bs.module "@cycle/react-dom"] external selectSel: hDomMVI = "select";
[@bs.module "@cycle/react-dom"] external small: hDom = "small";
[@bs.module "@cycle/react-dom"] external smallSel: hDomMVI = "small";
[@bs.module "@cycle/react-dom"] external source: hDom = "source";
[@bs.module "@cycle/react-dom"] external sourceSel: hDomMVI = "source";
[@bs.module "@cycle/react-dom"] external span: hDom = "span";
[@bs.module "@cycle/react-dom"] external spanSel: hDomMVI = "span";
[@bs.module "@cycle/react-dom"] external strong: hDom = "strong";
[@bs.module "@cycle/react-dom"] external strongSel: hDomMVI = "strong";
[@bs.module "@cycle/react-dom"] external style: hDom = "style";
[@bs.module "@cycle/react-dom"] external styleSel: hDomMVI = "style";
[@bs.module "@cycle/react-dom"] external sub: hDom = "sub";
[@bs.module "@cycle/react-dom"] external subSel: hDomMVI = "sub";
[@bs.module "@cycle/react-dom"] external sup: hDom = "sup";
[@bs.module "@cycle/react-dom"] external supSel: hDomMVI = "sup";
[@bs.module "@cycle/react-dom"] external table: hDom = "table";
[@bs.module "@cycle/react-dom"] external tableSel: hDomMVI = "table";
[@bs.module "@cycle/react-dom"] external tbody: hDom = "tbody";
[@bs.module "@cycle/react-dom"] external tbodySel: hDomMVI = "tbody";
[@bs.module "@cycle/react-dom"] external td: hDom = "td";
[@bs.module "@cycle/react-dom"] external tdSel: hDomMVI = "td";
[@bs.module "@cycle/react-dom"] external textarea: hDom = "textarea";
[@bs.module "@cycle/react-dom"] external textareaSel: hDomMVI = "textarea";
[@bs.module "@cycle/react-dom"] external tfoot: hDom = "tfoot";
[@bs.module "@cycle/react-dom"] external tfootSel: hDomMVI = "tfoot";
[@bs.module "@cycle/react-dom"] external th: hDom = "th";
[@bs.module "@cycle/react-dom"] external thSel: hDomMVI = "th";
[@bs.module "@cycle/react-dom"] external thead: hDom = "thead";
[@bs.module "@cycle/react-dom"] external theadSel: hDomMVI = "thead";
[@bs.module "@cycle/react-dom"] external time: hDom = "time";
[@bs.module "@cycle/react-dom"] external timeSel: hDomMVI = "time";
[@bs.module "@cycle/react-dom"] external title: hDom = "title";
[@bs.module "@cycle/react-dom"] external titleSel: hDomMVI = "title";
[@bs.module "@cycle/react-dom"] external tr: hDom = "tr";
[@bs.module "@cycle/react-dom"] external trSel: hDomMVI = "tr";
[@bs.module "@cycle/react-dom"] external u: hDom = "u";
[@bs.module "@cycle/react-dom"] external uSel: hDomMVI = "u";
[@bs.module "@cycle/react-dom"] external ul: hDom = "ul";
[@bs.module "@cycle/react-dom"] external ulSel: hDomMVI = "ul";
[@bs.module "@cycle/react-dom"] external video: hDom = "video";
[@bs.module "@cycle/react-dom"] external videoSel: hDomMVI = "video";