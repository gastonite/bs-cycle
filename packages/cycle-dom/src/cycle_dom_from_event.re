/* TODO: */

type any;
type predicate = Dom.event => bool;

type preventDefaultOpt = [
  | `PreventDefaultOpt(bool)
  | `Predicate(predicate)
  | `Comparator(Js_dict.t(any))
];