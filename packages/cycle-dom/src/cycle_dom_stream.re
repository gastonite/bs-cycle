module Node_stream =
  Xstream.Stream.Make_stream({
    type ok = Snabbdom.vnode;
    type error = Js.Exn.t;
  });

module Elements_stream =
  Xstream.Memory.Make_memory_stream({
    type ok = Js.Array.t(Snabbdom.vnode);
    type error = Js.Exn.t;
  });

module Events_stream =
  Xstream.Stream.Make_stream({
    type ok = Webapi.Dom.Event.t;
    type error = Js.Exn.t;
  });

[@bs.deriving abstract]
type eventsFnOptions = {
  [@bs.optional]
  useCapture: bool,
  [@bs.optional]
  passive: bool,
  [@bs.optional]
  bubbles: bool,
  [@bs.optional]
  preventDefault: Cycle_dom_from_event.preventDefaultOpt,
};