[@bs.deriving abstract]
type domDriverOptions = {
  [@bs.optional]
  modules: option(array(Snabbdom_module.t)),
};
type documentFragment = Dom.node;

type makeDomDriverWith('a) =
  ('a, domDriverOptions) => Cycle_dom_source.domSource;

[@bs.module "@cycle/dom/lib/es6/makeDOMDriver"]
external makeDOMDriverWithSelector: makeDomDriverWith(string) =
  "makeDOMDriver";
[@bs.module "@cycle/dom/lib/es6/makeDOMDriver"]
external makeDOMDriverWithElement: makeDomDriverWith(Dom.element) =
  "makeDOMDriver";
[@bs.module "@cycle/dom/lib/es6/makeDOMDriver"]
external makeDOMDriverWithDocumentFragment:
  makeDomDriverWith(documentFragment) =
  "makeDOMDriver";

/* Polymorphic version */
[@bs.module "@cycle/dom/lib/es6/makeDOMDriver"]
external makeDOMDriver:
  (
    [@bs.unwrap] [
      | `Sel(string)
      | `Element(Dom.element)
      | `Fragment(documentFragment)
    ],
    domDriverOptions
  ) =>
  Cycle_dom_source.domSource =
  "makeDOMDriver";