/* TODO: */
type fantasyObservable;
type eventObservable = Js_dict.t(fantasyObservable);
type domConfig = Js_dict.t(eventObservable);

[@bs.module "@cycle/dom"]
external mockDOMSource: domConfig => Cycle_dom_source.domSource = "";